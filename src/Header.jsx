import "./index.css"
import React from "react"
import { Input, Menu } from "antd"
import logo from "./img/logo.jpg"
import bg from "./img/bg.png"
import { Link } from "react-router-dom"

const { Search } = Input
class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      date: new Date(),
      current: "index",
    }
  }
  menuclick = (e) => {
    this.setState({ current: e.key })
  }
  render() {
    return (
      <div className='header'>
        <div className='title clearbox'>
          <div className='fl'>
            <img src={logo} alt='logo' title='公司logo' />
            <h1>天气数据分析系统</h1>
            <p>{this.state.date.toLocaleDateString()}</p>
          </div>
          <div className='fr'>
            <Search placeholder='请搜索天气数据' enterButton />
          </div>
        </div>
        <div className='bg'>
          <img src={bg} alt='bg' />
        </div>
        <div className='nav'>
          <Menu mode='horizontal' selectedKeys={this.state.current} onClick={this.menuclick}>
            <Menu.Item key='index'>
              <Link to='/'>首页</Link>
            </Menu.Item>
            <Menu.Item key='tqxw'>
              <Link to='/news'>天气新闻</Link>
            </Menu.Item>
            <Menu.Item key='tqyb'>
              <Link to='/weather'>天气预报</Link>
            </Menu.Item>
            <Menu.Item key='kqzl'>
              <Link to='/quality'>空气质量</Link>
            </Menu.Item>
            <Menu.Item key='xxtfx'>
              <Link to='/echartxx'>线型图分析</Link>
            </Menu.Item>
            <Menu.Item key='zztfx'>
              <Link to='/echartzz'>柱状图分析</Link>
            </Menu.Item>
            <Menu.Item key='bztfx'>
              <Link to='/echartbz'>饼状图分析</Link>
            </Menu.Item>
            <Menu.Item key='ldtfx'>
              <Link to='/echartld'>漏斗图分析</Link>
            </Menu.Item>
          </Menu>
        </div>
      </div>
    )
  }
}

export default Header
