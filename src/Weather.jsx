import React from "react"
import axios from "axios"
import "./index.css"
import { Select, Button, Breadcrumb } from "antd"
import "./weather.css"

class Weather extends React.Component {
  constructor(props) {
    super(props)
    this.tabs = React.createRef()
    this.state = {
      provarr: [],
      cityarr: [],
      cityname: "",
      cname: "",
      str: "",
    }
  }
  componentDidMount() {
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/province")
      .then((res) => {
        // console.log(res)
        th.setState({ provarr: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  componentWillUnmount() {
    this.setState = () => false
  }
  selcity = (values) => {
    // console.log(values)
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/city", {
        params: { id: values },
      })
      .then((res) => {
        // console.log(res)
        th.setState({ cityarr: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  selcityname = (values, option) => {
    this.setState({
      cityname: values,
      cname: option.children,
    })
    // console.log(option)
  }
  selweather = () => {
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/towea", {
        params: { cityname: th.state.cityname },
      })
      .then((res) => {
        console.log(res)
        let data = res.data.replace(/\/legend/g, "http://www.tianqihoubao.com/legend")
        th.tabs.current.innerHTML = data
        console.log(th.tabs)
      })
      .catch((e) => {
        console.log(e)
      })
  }
  render() {
    const { Option } = Select
    let pros = this.state.provarr.map((value, index) => {
      return (
        <Option key={value.id} value={value.id}>
          {value.pname}
        </Option>
      )
    })
    let citys = this.state.cityarr.map((value, index) => {
      return (
        <Option key={value.id} value={value.cityname}>
          {value.cname}
        </Option>
      )
    })
    return (
      <div className='wrap content tqybwrap'>
        <div className='bread'>
          <Breadcrumb separator='>'>
            <Breadcrumb.Item>天气数据分析系统</Breadcrumb.Item>
            <Breadcrumb.Item>天气预报页面</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className='selwrap'>
          <span className='sel_item'>省：&nbsp;</span>
          <Select onChange={this.selcity} className='select'>
            {pros}
          </Select>
          <span className='sel_item'>城市：&nbsp;</span>
          <Select onChange={this.selcityname} className='select'>
            {citys}
          </Select>
          <Button type='primary' onClick={this.selweather} className='sel_btn'>
            查询
          </Button>
        </div>
        <h1>{this.state.cname}天气预报</h1>
        <p>数据为中央气象局发布的24小时天气预报</p>
        <div ref={this.tabs} className='tabs'></div>
      </div>
    )
  }
}

export default Weather
