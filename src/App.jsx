import "./index.css"
import React from "react"
import Footer from "./Footer"
import Header from "./Header"
import Home from "./Home"
import { useNavigate, Routes, Route } from "react-router-dom"
import Login from "./Login"
import Res from "./Res"
import Passr from "./Passr"
import News from "./News"
import Weather from "./Weather"
import Quality from "./Quality"
import Echartxx from "./Echartxx"
import Echartzz from "./Echartzz"
import Echartbz from "./Echartbz"
import Echartld from "./Echartld"

function App() {
  const nav = useNavigate()
  const toIndex = (value) => {
    localStorage.setItem("names", value)
    nav("/")
  }
  const toLogin = () => {
    nav("/login")
  }
  return (
    <div className='App'>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/login' element={<Login toIndex={toIndex} />} />
        <Route path='/res' element={<Res toLogin={toLogin} />} />
        <Route path='/passr' element={<Passr toLogin={toLogin} />} />
        <Route path='/news' element={<News />} />
        <Route path='/weather' element={<Weather />} />
        <Route path='/quality' element={<Quality />} />
        <Route path='/echartxx' element={<Echartxx />} />
        <Route path='/echartzz' element={<Echartzz />} />
        <Route path='/echartbz' element={<Echartbz />} />
        <Route path='/echartld' element={<Echartld />} />
      </Routes>
      <Footer />
    </div>
  )
}

export default App
